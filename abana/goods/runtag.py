#coding=utf8
import sys
sys.path.append('/home/lianyijiang/src/goods_tag/')
from stepone.db import get_conn
from stepone.tag import commit
from stepone.goods_gene import dump_style_words
from optparse import OptionParser


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-t", "--task", dest="task",
                      help="task info", metavar="TASK")
    (options, args) = parser.parse_args()
    task= options.task
    if task == "gtag":
        conn = get_conn()
        # gentags(conn, "./res/tags")
        commit(conn, "/home/lianyijiang/src/res/tags", '/home/lianyijiang/outtag')
    elif task == 'gene':
        # dump_style_words('材质')
        # dump_style_words('款式')
        dump_style_words('品牌')
