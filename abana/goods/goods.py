#coding=utf8
from abana.goods.db import get_conn

def get_brands(rbar=1):
    conn = get_conn()
    cur = conn.cursor()
    sql = 'select brand_name, rank from r_brand_id_name_all where rank > %s' %(rbar)
    cur.execute(sql)
    res = cur.fetchall()
    fin = {}
    for bname, brank in res:
        names = bname.split('/')
        for name in names:
            fin[name] = brank
    return fin

def get_category_name():
    conn = get_conn()
    cur = conn.cursor()
    sql = 'select taobao_cid, name from category'
    cur.execute(sql)
    res = cur.fetchall()
    return dict(res)

def get_commodity_category():
    conn = get_conn()
    cur = conn.cursor()
    sql = 'select id,taobao_cid from goods where taobao_cid !=0'
    cur.execute(sql)
    res = cur.fetchall()
    return dict(res)

def get_commodity_attrs():
    conn = get_conn()
    cur = conn.cursor()
    sql = 'select id, attributes from goods where taobao_cid !=0'
    cur.execute(sql)
    res = cur.fetchall()
    return dict(res)

def parse_attrs(attrs_txt, keys=['款式', '品牌']):
    # 获取款式和品牌
    ss = [i.split(':') for i in attrs_txt.split('\n')]
    iattrs = dict(filter(lambda i: len(i) == 2, ss))
    candi = [iattrs.get(i, '') for i in keys]
    x = filter(lambda i:i, candi)
    y = [i.lstrip(' ').strip() for i in x if i]
    return y

def get_df(infile):
    x = {}
    for line in open(infile):
        w, s = line.rstrip().split()
        x[w] = int(s)
    return x

def get_brands2(infile):
    x = {}
    for line in open(infile):
        w, s = line.rstrip().split()
        x[w] = int(s)
    return x
