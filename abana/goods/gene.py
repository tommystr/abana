#coding=utf8
from abana.goods.db import get_conn
from abana.goods.goods import parse_attrs

def dump_style_words(key):
    wset = set()
    conn = get_conn()
    cur = conn.cursor()
    sql = 'select id, attributes from goods where taobao_cid !=0'
    cur.execute(sql)
    res = cur.fetchall()
    for ind, (tid, attrs) in enumerate(res):
        if ind > 500:
            break
        x = parse_attrs(attrs, [key])
        if x and not x[0] in wset:
            print x[0]
            wset.add(x[0])
