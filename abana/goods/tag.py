#coding=utf8
import sys
import os
import difflib
from stepone.db import get_conn
from stepone.config import df_file, brand_file
from stepone import goods
from stepone.data import STOPWORDS
reload(sys)
sys.setdefaultencoding('utf8')
import jieba
import re

def gentags(conn, outfile):
    idir = os.path.dirname(outfile)
    last_file = os.path.join(idir, '.last_id')
    if os.path.exists(last_file):
        last_id = int(open(last_file).readlines()[0])
    else:
        last_id = 0
    jieba.load_userdict(nlp['dict'])
    swords = [i.strip() for i in open("./biaodian_stop.txt").readlines()]
    swords.extend('每个 ID 限购 件'.split())
    p = re.compile('^\d+$')
    cur = conn.cursor()
    cur.execute('select id, title from goods where id> %s', (last_id,))
    res = cur.fetchall()
    ofp = open(outfile,'w')
    for tid, title in res:
        last_id = tid
        rwords = jieba.cut(title)
        tags = []
        for i in rwords:
            if i == ' ' or p.match(i):
                continue
            if not i in STOPWORDS:
                tags.append(i)
        if tags:
            print >>ofp, "%s\t%s" %(tid,"|".join(tags))
    ofp.close()
    save_lastid(last_id, last_file)

def save_lastid(tid, ofile):
    ofp = open(ofile, 'w')
    print >>ofp, tid
    ofp.close()


def get_catewords(words, cnames, simbar=0.1):
    oset = set(cnames[:])
    olist = cnames[:]

    for i in words:
        # if i in bname:
        #     print 'brand',i, bname[i]
        # if i in cnames:
        #     print 'cate',i
        for j in cnames:
            dscore =difflib.SequenceMatcher(None, i.decode('utf8'), j.decode('utf8')).ratio()
            if dscore > simbar:
                lset_add(olist, oset, i)
    return olist

def lset_add(olist, oset, ele):
    if ele and not ele in oset:
        ele = ele.lstrip(' ')
        olist.append(ele)
        oset.add(ele)

def lset_extend(olist, oset, eles):
    for ele in eles:
        ele = ele.lstrip(' ')
        if ele and not ele in oset:
            olist.append(ele)
            oset.add(ele)

def extract_tags(words, cname, attrs, df, bname):
    # 从分词结果words，类目，还有属性信息中提取有用的tag出来
    cnames = []
    if cname:
        cnames = cname.split('/')
    # tags = sorted(rtags, key=lambda i: df.get(i, 0) * bname.get(i), reverse=True)
    # tags = '|'.join(tags)
    attr_tags = goods.parse_attrs(attrs, keys=['款式', '品牌'])
    attr_tags.append(cname)
    # print 'attrs',"|".join(attr_tags)
    cate_tags = get_catewords(words, cnames)
    # print 'cates',"|".join(cate_tags)
    oset = set()
    olist = []
    lset_extend(olist, oset, attr_tags + cate_tags)
    for w in words:
        if w and not w in STOPWORDS:
            if df.get(w, 1) > 100:
                lset_add(olist, oset, w)
            if bname.get(w, 1) > 100:
                lset_add(olist, oset, w)
    return olist

def commit(conn, infile, ofile):
    # 对分词后的结果分析出其中的有意义的tag出来
    fp = open(infile)
    ofp = open(ofile, 'w')
    cur = conn.cursor()
    item_cate = goods.get_commodity_category()
    item_attrs = goods.get_commodity_attrs()
    cid_cname = goods.get_category_name()
    df = goods.get_df(df_file)
    bname = goods.get_brands()

    nocid_num = 0
    for ind, line in enumerate(fp):
        if ind < 5705:
            continue
        if ind == 5800:
            break
        tid, words = line.rstrip().split()
        words = words.split('|')
        tid = int(tid)
        cid = item_cate.get(tid, None)
        attrs = item_attrs.get(tid, None)
        if not cid:
            print 'skip because cates: ', cid, tid, "|".join(words)            
            nocid_num += 1
            continue
        if not attrs:
            print 'skip because attrs: ', cid, tid, "|".join(words)            
            continue
        cname = cid_cname.get(cid, None)
        tags = extract_tags(words, cname, attrs, df, bname)
        print >>ofp, 'words: ',"|".join(words)
        tagstr = '|'.join(tags)
        print >>ofp, ind, '\t', tagstr, "\n"
        # sql = "INSERT INTO goods_tag (`goods_id`,`tags`) VALUES (%s, '%s') ON DUPLICATE KEY UPDATE `tags`='%s'" %(tid, tags, tags)
        # cur.execute(sql)
    conn.commit()
    cur.close()
    conn.close()

