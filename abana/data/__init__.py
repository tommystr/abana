import os
import glob
import codecs
DICTFILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'jiebadict.txt')


def get_stops():
    pwd = os.path.realpath(__file__)
    pattern = os.path.join(os.path.dirname(pwd), '*_stop.txt')
    words = set()
    for f in glob.glob(pattern):
        print f
        with codecs.open(f, encoding='utf8') as ofp:
            for line in ofp:
                w = line.rstrip()
                words.add(w)
    return words

STOPWORDSET = get_stops()

if __name__ == "__main__":
    stops = get_stops()
