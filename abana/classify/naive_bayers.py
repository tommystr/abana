# coding=utf-8
'''
naive bayes 分类器
输入: item_cate, segpath, 其中segpath 是fset格式
self.default_prop[cate]=float(1)/(cwords_n[cate]+vocabulary_n)
self.cate_prior_prop[cate]=float(self.cate_count[cate])/total
'''
from .tools import get_fnms, make_sure_dir, line_parse_idText
from collections import defaultdict
import os
import codecs


class NaiveBayers:

    def get_item_cate(self, train_cate):
        self.item_cate = {}
        for line in open(train_cate):
            tid, cate = map(int, line.rstrip().split())
            self.item_cate[tid] = cate

    def get_cate_count(self):
        self.cate_count = defaultdict(int)
        for item, cate in self.item_cate.iteritems():
            self.cate_count[cate] += 1
        total = sum(self.cate_count.itervalues())
        self.cate_prior_prop = {}
        for cate in self.cate_count.iterkeys():
            self.cate_prior_prop[cate] = float(self.cate_count[cate]) / total

    def train(self, train_vsm, train_cate):
        # 训练得到模型
        self.get_item_cate(train_cate)
        self.get_cate_count()
        self.prepare(train_vsm)

    def prepare(self, segpath):
        fnms = get_fnms(segpath)
        vocabulary = set()
        vocabulary_n = 0
        # word的标号，内部全部是使用数字
        self.word_num = {}
        self.num_word = {}
        self.word_cates = defaultdict(set)
        # word 和哪些cates想关联
        cate_wc = {}
        cate_wn = {}
        for fnm in fnms:
            for line in codecs.open(fnm, encoding='utf8'):
                try:
                    tid, v = line.rstrip().split('\t')
                except:
                    continue
                tid = int(tid)
                words = v.split('|')
                if tid not in self.item_cate:
                    continue
                cate = self.item_cate[tid]
                for w in words:
                    if w not in vocabulary:
                        self.word_num[w] = vocabulary_n
                        self.num_word[vocabulary_n] = w
                        vocabulary.add(w)
                        vocabulary_n += 1
                    num = self.word_num[w]
                    self.word_cates[num].add(cate)
                    cate_wc.setdefault(cate, defaultdict(int))
                    cate_wc[cate][num] += 1

        for w, cates in self.word_cates.iteritems():
            self.word_cates[w] = sorted(cates)

        cate_wn = {}
        for cate, wc in cate_wc.iteritems():
            cate_wn[cate] = sum(wc.itervalues())

        self.cate_word_prop = {}
        for cate, wc in cate_wc.iteritems():
            self.cate_word_prop[cate] = dict([[w, float(wc[w] + 1) / (cate_wn[cate] + vocabulary_n)]
                                              for w in wc.iterkeys()])

        self.default_prop = {}
        # 对于没有在给定的cate类出现的词，默认的概率
        for cate in cate_wn.iterkeys():
            self.default_prop[cate] = 1.0 / (cate_wn[cate] + vocabulary_n)

    def output_model(self, outpath):
        cwp = os.path.join(outpath, "cate_word_prop")
        make_sure_dir(cwp)
        ofp = open(cwp, "w")
        for cate, word_prop in self.cate_word_prop.iteritems():
            print >>ofp, "%s\t%s" % (cate, "|".join(["%s:%s" % (w, s) for w, s in word_prop.iteritems()]))
        ofp.close()

        cwp = os.path.join(outpath, "cate_word_prop_show")
        ofp = codecs.open(cwp, "w", encoding='utf8')
        for cate, word_prop in self.cate_word_prop.iteritems():
            print >>ofp, "%s\t%s" % (cate, "|".join(["%s:%s" % (self.num_word[w], s) for w, s in word_prop.iteritems()]))
        ofp.close()

        ofp = open(os.path.join(outpath, "word_cates"), 'w')
        for word, cates in self.word_cates.iteritems():
            print >>ofp, "%s\t%s" % (word, "|".join(map(str, cates)))
        ofp.close()

        def __dumpdict(dic, fnm):
            ofp = codecs.open(fnm, "w", encoding='utf8')
            for k, v in dic.iteritems():
                print >>ofp, "%s\t%s" % (k, v)
            ofp.close()

        __dumpdict(self.default_prop, os.path.join(outpath, "default_prop"))
        __dumpdict(self.cate_prior_prop, os.path.join(outpath, "cate_prior_prop"))
        __dumpdict(self.word_num, os.path.join(outpath, "word_num"))

    def read_model(self, inpath):
        cwp = os.path.join(inpath, "cate_word_prop")
        fp = codecs.open(cwp, encoding='utf8')
        self.cate_word_prop = {}
        for line in fp:
            docid, wordsinfo = line.rstrip().split("\t")
            wordsinfo = [i.split(":") for i in wordsinfo.split("|")]
            cate = int(docid)
            word_prop = [[int(i[0]), float(i[1])] for i in wordsinfo]
            self.cate_word_prop[cate] = dict(word_prop)
        fp.close()

        fp = open(os.path.join(inpath, "word_cates"))
        self.word_cates = {}
        for line in fp:
            word, cates = line.rstrip().split("\t")
            word = int(word)
            cates = map(int, cates.split('|'))
            self.word_cates[word] = cates
        fp.close()

        def __read_dict(fnm, type_a, type_b):
            x = {}
            ofp = codecs.open(fnm, encoding='utf8')
            for line in ofp:
                cate, prop = line.split('\t')
                x[type_a(cate)] = type_b(prop)
            ofp.close()
            return x

        self.default_prop = __read_dict(os.path.join(inpath, "default_prop"), int, float)
        self.cate_prior_prop = __read_dict(os.path.join(inpath, "cate_prior_prop"), int, float)
        self.word_num = __read_dict(os.path.join(inpath, "word_num"), unicode, int)
        self.num_word = {}
        for w, n in self.word_num.iteritems():
            self.num_word[n] = w
        self.cates = sorted(self.default_prop.keys())

    def get_cate(self, words, topk, with_prior, refuse_predict):
        cate_props = []
        # 只对有可能的类来求类目的似然概率, 类目过多的时候可以减少不必要的计算
        possible_cates = set()
        for word in words:
            if word in self.word_num:
                cates = self.word_cates[self.word_num[word]]
                possible_cates.update(cates)

        if not possible_cates:
            if refuse_predict:
                return [[-1, -1]]
            else:
                return sorted(self.cate_prior_prop.iteritems(), key=lambda k: k[1], reverse=True)[:topk]
        else:
            for cate in possible_cates:
                if with_prior:
                    prop = self.cate_prior_prop[cate]
                else:
                    prop = 1.0
                for word in words:
                    if (word not in self.word_num) or (self.word_num[word] not in self.cate_word_prop[cate]):
                        prop *= self.default_prop[cate]
                    else:
                        prop *= self.cate_word_prop[cate][self.word_num[word]]
                cate_props.append([cate, prop])
            return sorted(cate_props, key=lambda k: k[1], reverse=True)[:topk]

    def predict(self, inpath, outpath, topk=1, with_prior=True, refuse_predict=False):
        # inpath should be seg form
        fnms = get_fnms(inpath)
        make_sure_dir(outpath)
        ofp = codecs.open(outpath, 'w', encoding='utf8')
        for fnm in fnms:
            fp = codecs.open(fnm, encoding='utf8')
            for line in fp:
                tid, words = line_parse_idText(line)
                words = words.split('|')
                cate_score = self.get_cate(words, topk, with_prior, refuse_predict)
                ostr = "%s\t%s" %(tid, "|".join(["%s:%s" %(w, s) for w, s in cate_score]))
                print >>ofp, ostr
        fp.close()
        ofp.close()

    def predict_show(self, inpath, outpath, cate_name, topk=1, with_prior=True, refuse_predict=False):
        # inpath should be seg form
        fnms = get_fnms(inpath)
        make_sure_dir(outpath)
        ofp = codecs.open(outpath, 'w', encoding='utf8')
        for fnm in fnms:
            fp = codecs.open(fnm, encoding='utf8')
            for line in fp:
                tid, rwords = line_parse_idText(line)
                words = rwords.split('|')
                cate_score = self.get_cate(words, topk, with_prior, refuse_predict)
                ostr = "%s\t%s\t%s" %(tid, rwords, 
                                      "|".join(["%s:%s" %(cate_name.get(c, None), s) for c, s in cate_score]))
                print >>ofp, ostr
        fp.close()
        ofp.close()


    def online_predict(self, words, topk=1, with_prior=False, refuse_predict=False):
        # with_prior, 是否使用先验概率
        # refuse_predict 对于没有收录词的时候，是否拒绝判断
        # inpath should be seg form
        # 注意words 不可以是迭代器, 因为在判断类型的时候需要用多次
        cate_prob = self.get_cate(words, topk, with_prior, refuse_predict)
        return cate_prob

    def show_onecate(self, cate_id):
        word_prob = self.cate_word_prop[cate_id]
        print "#" * 3
        print self.cate_prior_prop[cate_id]
        print "|".join(["%s:%s" %(self.num_word[i], j)
                        for i, j in sorted(word_prob.iteritems(), key=lambda i:i[1], reverse=True)])
