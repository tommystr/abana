import glob,os,sys,inspect

def make_sure_dir(fnm):
    dirname=os.path.dirname(fnm)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

def get_dict(fnm):
    d={}
    for line in open(fnm):
        x,y = map(int, line.rstrip().split())
        d[x]=y
    return d
        
def dump_dict(fnm,d):
    make_sure_dir(fnm)
    ofp=open(fnm,"w")
    for k,v in d.iteritems():
        print >>ofp,"%s\t%s" %(k,v)
    ofp.close()

def split_arr(arr,chunk_size):
    l=len(arr)
    ly=l/chunk_size
    res=[]
    for i in range(ly):
        start=chunk_size*i
        end=start+chunk_size
        res.append(arr[start:end])
    others=arr[(ly*chunk_size):]
    if others:
        res.append(others)
    return res

def map_on_dict(arr,d):
    x=[]
    for i in arr:
        if d.has_key(i):
            x.append((i,d[i]))
    return x

def map_on_dict_r(arr,d):
    x=[]
    for i in arr:
        if d.has_key(i):
            x.append((d[i],i))
    return x

def line_parse_idText(line,sep="\t",raw=False):
    line = line.rstrip()
    ind=line.index(sep)
    did=line[0:ind]
    text=line[(ind+1):]
    if raw:
        return (did,text)
    else:
        try:
            return (int(did),text)
        except:
            print line
            exit(2)

def del_line_breaks(line):
    return line.replace("\n"," ").replace("\r"," ")


class LineParseError(Exception):
        def __init__(self, value):
                self.value = value
        def __str__(self):
                return self.value

def line_parse(line,form):
    if form=="fset":
        lid,words=line.rstrip().split()
        return (int(lid),map(int,words.split("|")))
    elif form=="fs" or form=="sim":
        lid,words=line.rstrip().split()
        wordsinfo=[i.split(":") for i in wordsinfo.split("|")]
        return (int(docid),[[int(i),float(f)] for i,f in wordsinfo])
    else:
        raise Exception("error in line_parse")

def line_parse_sim(line,tag=False):
    try:
        docid,wordsinfo=line.rstrip().split("\t")
        wordsinfo=[i.split(":") for i in wordsinfo.split("|")]
        if tag:
            return (int(docid),[[int(i),f] for i,f in wordsinfo])
        else:
            return (int(docid),[[int(i),float(f)] for i,f in wordsinfo])
    except:
        raise LineParseError("error in line_parse_sim %s" %(line))

def dir_dict(commdir,istr,funname=''):
    return dict([(i,os.path.join(commdir,funname,i)) for i in istr.split()])

def spoor():
     print "calling ",inspect.stack()[1][3]

def read_sim(sim_path):
    d={}
    if os.path.isdir(sim_path):
        files=glob.glob(sim_path+"/*")
    else:
        files=[sim_path]
    for f in files:
        fp=open(f)
        for line in fp:
            docid,wordsinfo=line_parse_sim(line)
            docid=int(docid)
            local_d={}
            for wid,score in wordsinfo:
                local_d[int(wid)]=float(score)
            d[docid]=local_d
        fp.close()
    return d

def read_simlist(sim_path):
    d={}
    if os.path.isdir(sim_path):
        files=glob.glob(sim_path+"/*")
    else:
        files=[sim_path]
    for f in files:
        fp=open(f)
        for line in fp:
            docid,wordsinfo=line_parse_sim(line)
            d[docid]=wordsinfo
        fp.close()
    return d

def sim_form(word_score,orderByScore=True,digits=None):
    if isinstance(word_score,list):
        obj=word_score
    elif isinstance(word_score,dict):
        obj=list(word_score.iteritems())
    else:
        raise Exception('bad input for sim_form')
    if orderByScore:
        word_score=sorted(obj,key=lambda x:x[1],reverse=True)
    else:
        word_score=obj
    if digits:
        pattern="%%s:%%.%sf" %(digits)
        wds=[pattern %(word,score) for word,score in word_score]
    else:
        wds=["%s:%s" %(word,info) for word,info in word_score]
    return "|".join(wds)

def del_blank(line):
    return line.replace("\n","").replace("\r","").replace(" ","").replace("\t","")

def get_fnms(inpath):
    import glob,os
    if os.path.isdir(inpath):
        fnms= glob.glob(inpath+"/*")
    else:
        fnms=[inpath]
    return sorted(fnms)

def word_count(words):
  word_count={}
  for word in words:
    word_count.setdefault(word,0)
    word_count[word]+=1
  return word_count

