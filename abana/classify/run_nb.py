import sys
sys.path.append('/home/lianyijiang/Projects/abana/')
import codecs
from abana.classify.naive_bayers import NaiveBayers
from abana.classify.tools import make_sure_dir, dir_dict, del_line_breaks
from abana.classify.jseg import jseg
from abana.goods.db import get_conn
from abana.goods.goods import get_category_name
from datetime import datetime


def prepare_train(id_title, id_cate):
    make_sure_dir(id_title)
    make_sure_dir(id_cate)
    ofp = codecs.open(id_title, 'w', encoding='utf8')
    ofpc = codecs.open(id_cate, 'w', encoding='utf8')
    conn = get_conn()
    cur = conn.cursor()
    sql = 'select id, title, taobao_cid from goods where platform_id in (1, 2) and taobao_cid != 0'
    cur.execute(sql)
    res = cur.fetchall()
    for tid, title, cid in res:
        print >>ofp, '%s\t%s' %(tid, del_line_breaks(title))
        print >>ofpc, '%s\t%s' %(tid, cid)
    ofp.close()
    ofpc.close()

def prepare_predict(outpath):
    make_sure_dir(outpath)
    ofp = codecs.open(outpath, 'w', encoding='utf8')
    conn = get_conn()
    cur = conn.cursor()
    sql = 'select id, title from goods where platform_id > 2'
    cur.execute(sql)
    res = cur.fetchall()
    for tid, title in res:
        print >>ofp, '%s\t%s' %(tid, del_line_breaks(title))
    ofp.close()

def run(bdir):
    nb = NaiveBayers()
    dirs = dir_dict(bdir, 'title_tr title_te seg_tr seg_te cate_tr model out out_show')
    # prepare_train(dirs['title_tr'], dirs['cate_tr'])
    # prepare_predict(dirs['title_te'])
    # jseg(dirs['title_tr'], dirs['seg_tr'])
    # jseg(dirs['title_te'], dirs['seg_te'])
    # nb.train(dirs['seg_tr'], dirs['cate_tr'])
    # nb.output_model(dirs['model'])
    nb.read_model(dirs['model'])
    cname = get_category_name()
    # nb.predict_show(dirs['seg_te'], dirs['out_show'], cname, topk=3, with_prior=False, refuse_predict=False)
    nb.predict_show(dirs['seg_te'], dirs['out'], cname, topk=3, with_prior=False, refuse_predict=False)
    dump2sqlite(dirs['out'])

def dump2sqlite(infile):
    database = '/home/lianyijiang/Rails/moony/db/development.sqlite3'
    import sqlite3
    con = sqlite3.connect(database)
    cursor = con.cursor()
    for line in open(infile):
        tid, info = line.rstrip().split()
        sql="insert or ignore into audit_commodity_categories (goods_id, candidates, audit_user_id, taobao_cid) \
        values ('%s','%s','%s','%s')"%(int(tid), info, 2, 0)
        cursor.execute(sql)
    con.commit()


if __name__ == "__main__":
    bdir = '/home/lianyijiang/data/nb'
    run(bdir)
