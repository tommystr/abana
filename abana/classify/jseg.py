# coding=utf8
from abana.data import STOPWORDSET, DICTFILE
from abana.classify.tools import make_sure_dir, line_parse_idText
import jieba
import re
import codecs
import regex
""" 分词之前先把文本normalize
"""


def jseg(infile, outfile):
    jieba.load_userdict(DICTFILE)
    make_sure_dir(outfile)
    p = re.compile('^\d+$')
    pempty = re.compile('^\s+$')
    english_pattern = regex.compile('^\w+$')

    ofp = codecs.open(outfile, 'w', encoding='utf8')
    fp = codecs.open(infile, encoding='utf8')
    for line in fp:
        try:
            tid, title = line_parse_idText(line)
            otitle = ''
            for i in title:
                if english_pattern.match(i):
                    otitle += i.lower()
                else:
                    otitle += i
            title = otitle
        except:
            continue
        rwords = jieba.cut(title)
        owords = []
        for i in rwords:
            if pempty.match(i) or p.match(i) or i in STOPWORDSET:
                continue
            else:
                owords.append(i)
        if owords:
            print >>ofp, '%s\t%s' % (tid, '|'.join(owords))
    ofp.close()
    fp.close()
